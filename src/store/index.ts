import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import logger from 'redux-logger';

import reducer from '../reducers';
import rootSaga from '../sagas';

function saveToLocalStorage(state: any) {
  try {
    let newState = {...state};
    newState = {
      favourite: newState.favourite,
    };
    const serializedState = JSON.stringify(newState);
    localStorage.setItem('state', serializedState);
  } catch (e) {
    // statements
    console.log(e);
  }
}

function loadFromLocalStorage() {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) return undefined;
    return JSON.parse(serializedState);
  } catch (e) {
    // statements
    console.log(e);
    return undefined;
  }
}

const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducer,
  persistedState,
  applyMiddleware(sagaMiddleware, logger)
)

sagaMiddleware.run(rootSaga);

store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;
