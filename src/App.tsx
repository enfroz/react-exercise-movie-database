import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom';

import { Button } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import Search from './pages/Search';
import Detail from './pages/Detail';
import Favourites from './pages/Favourites';

function App({ classes }: WithStyles) {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Search} />
          <Route path="/detail/:id" component={Detail} />
          <Route path="/favourites" component={Favourites} />
        </Switch>
        <Link to="/favourites">
          <Button variant="contained" className={classes.button}>
            Favourites
          </Button>
        </Link>
      </Router>
    </div>
  );
}

export default withStyles((theme) => ({
  button: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
}))(App);
