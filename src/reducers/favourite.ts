import { ADD_ITEM, REMOVE_ITEM } from '../actions/favourite';

export const INITIAL_STATE = {
  items: [],
};

const favourite = (state = INITIAL_STATE, action: any) => {
  const { type } = action;

  switch (type) {
    case ADD_ITEM:
      return {
        ...state,
        items: [
          ...state.items,
          {
            id: action.id,
            title: action.title,
            poster: action.poster,
          },
        ],
      };
    case REMOVE_ITEM:
      const newArray = state.items.filter((item: any) => item.id !== action.id);
      return {
        ...state,
        items: [
          ...newArray,
        ]
      };
    default:
      return state;
  }
};

export default favourite;
