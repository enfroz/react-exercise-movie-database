import { combineReducers } from 'redux';

import favourite from './favourite';
import movies from './movies';

export default combineReducers({
  favourite,
  movies,
});