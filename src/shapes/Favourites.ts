interface Favourites {
  id: string;
  title: string;
  poster: string;
}

export default Favourites;
