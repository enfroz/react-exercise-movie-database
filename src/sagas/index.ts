import { put, takeLatest, all } from 'redux-saga/effects';

function* fetchMovies(params: any) {
  console.log(params);
  const json = yield fetch(`${process.env.REACT_APP_ENDPOINT}?apikey=${process.env.REACT_APP_API_KEY}&s=${params.string}&page=${params.page}`)
    .then(response => response.json());
  yield put({ type: 'MOVIE_RECEIVED', payload: json });
}

function* fetchMovieById(params: any) {
  console.log(params);
  const json = yield fetch(`${process.env.REACT_APP_ENDPOINT}?apikey=${process.env.REACT_APP_API_KEY}&i=${params.id}`)
    .then(response => response.json());
  yield put({ type: 'MOVIE_BY_ID_RECEIVED', payload: json });
}

function* actionWatcher() {
  yield takeLatest('FETCH_MOVIES', fetchMovies);
  yield takeLatest('FETCH_MOVIE_BY_ID', fetchMovieById);
}

export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}
