export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';

export function addToFavourites(id: string, title: string, poster: string) {
  return {
    type: ADD_ITEM,
    id,
    title,
    poster,
  };
}

export function removeFromFavourites(id: string) {
  return {
    type: REMOVE_ITEM,
    id,
  };
}
