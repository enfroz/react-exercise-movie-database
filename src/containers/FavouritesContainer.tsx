import { connect } from 'react-redux';

import Favourites from '../components/Favourites';

import { removeFromFavourites } from '../actions/favourite';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    favourites: state.favourite.items,
  };
}

export default connect(
  mapStateToProps,
  {
    removeFromFavourites,
  },
)(Favourites);
