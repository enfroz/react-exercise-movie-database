import { connect } from 'react-redux';

import SearchBox from '../components/SearchBox';

import { fetchMovies } from '../actions/movies';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    searchString: state.movies.searchString,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchMovies,
  },
)(SearchBox);
