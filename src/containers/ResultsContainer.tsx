import { connect } from 'react-redux';

import Results from '../components/Results';

import { fetchMovies } from '../actions/movies';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    ...state.movies,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchMovies,
  },
)(Results);
