import { connect } from 'react-redux';

import Detail from '../components/Detail';

import { fetchMovieById } from '../actions/movies';
import { addToFavourites } from '../actions/favourite';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    item: state.movies.movieDetail,
    favourites: state.favourite.items,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchMovieById,
    addToFavourites,
  },
  )(Detail);
