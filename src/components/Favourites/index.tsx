import React from 'react';
import { Link } from 'react-router-dom';

import { Box, Button, Typography } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import FavouritesShape from '../../shapes/Favourites';

type Props = {
  favourites: FavouritesShape[];
  removeFromFavourites: (id: string) => void;
};

function Favourites({ classes, favourites, removeFromFavourites }: Props & WithStyles) {
  return (
    <Box position="relative" mt={2}>
      <Box padding={5} textAlign="center">
        <Typography component="h1" variant="h3">Your favourite movies</Typography>
      </Box>
      <Box position="absolute" left={0} top={0}>
        <Link to={`/`} className={classes.backLink}>
          <Button color="primary">back to search</Button>
        </Link>
      </Box>
      <Box display="flex" justifyContent="center" pt={10}>
        <Box display="flex" flexDirection="column" width={600}>
          {!favourites.length ? (
            <Box display="flex" alignItems="center" justifyContent="center">
              <Typography>You have no favourite movies yet</Typography>
            </Box>
          ) : (
            <>
              {favourites.map((item: FavouritesShape) => (
                <Box key={item.id} className={classes.itemWrapper}>
                  <Box className={classes.posterWrapper}>
                    {item.poster !== "N/A" && (
                      <img src={item.poster} alt={item.title} className={classes.poster} />
                    )}
                  </Box>
                  <span>{item.title}</span>
                  <Box className={classes.buttonWrappers}>
                    <Link to={`/detail/${item.id}`} className={classes.link}>
                      <Button variant="contained" color="secondary">Detail</Button>
                    </Link>
                    <Button
                      onClick={() => removeFromFavourites(item.id)}
                      variant="text"
                      className={classes.button}
                    >
                      Remove
                    </Button>
                  </Box>
                </Box>
              ))}
            </>
          )}
        </Box>
      </Box>
    </Box>
  );
}

export default withStyles((theme) => ({
  itemWrapper: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2),
    paddingRight: theme.spacing(30),
    position: 'relative',
    marginBottom: theme.spacing(0.5),
    marginTop: theme.spacing(0.5),
    boxShadow: '1px 1px 10px rgba(0, 0, 0, 0.15)',
  },
  posterWrapper: {
    width: 50,
    height: 50,
    overflow: 'hidden',
  },
  poster: {
    width: 'auto',
    height: '100%',
  },
  buttonWrappers: {
    position: 'absolute',
    right: theme.spacing(2),
  },
  link: {
    textDecoration: 'none',
  },
  button: {
    color: 'red',
  },
}))(Favourites);
