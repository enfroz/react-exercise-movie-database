import React, { useCallback, useState, useEffect } from 'react';

import { Box, TextField, Button } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/core/styles';

type Props = {
  fetchMovies: (string: string) => any;
  searchString: string;
};

function SearchBox({ classes, fetchMovies, searchString }: Props & WithStyles) {
  const [val, setVal] = useState('');

  const submitSearch = useCallback((e) => {
    e.preventDefault();
    fetchMovies(val);
    return false;
  }, [val, fetchMovies]);

  const handleVal = useCallback((e) => {
    setVal(e.target.value);
  }, []);

  useEffect(() => {
    if (searchString.length) setVal(searchString);
  }, []);

  return (
    <Box width={800} display="flex" justifyContent="center" margin="0 auto" pb={2} mt={2}>
      <form onSubmit={submitSearch} className={classes.form}>
        <TextField
          variant="outlined"
          placeholder="Search for movies..."
          value={val}
          onChange={handleVal}
          classes={{
            root: classes.textFieldRoot,
          }}
          inputProps={{
            classes: {
              root: classes.inputRoot,
              input: classes.inputInput,
            }
          }}
        />
        <Button type="submit" variant="contained" color="primary" onClick={submitSearch}>
          Search
        </Button>
      </form>
    </Box>
  );
}

export default withStyles((theme) => ({
  form: {
    width: '100%',
    display: 'flex',
  },
  textFieldRoot: {
    width: '100%',
    marginRight: theme.spacing(1),
  },
  inputRoot: {
    width: '100%',
  },
  inputInput: {
    width: '100%',
  },
}))(SearchBox);

