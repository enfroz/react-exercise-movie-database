import React from 'react';
import classNames from 'classnames';
import { times } from 'lodash';

import { withStyles, WithStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

type Props = {
  totalResults: number;
  rowsPerPage: number;
  page: number;
  setPage: (page: number) => void;
};

function Pagination({
  classes,
  totalResults,
  rowsPerPage,
  page,
  setPage,
}: Props & WithStyles) {
  return (
    <Box display="flex" justifyContent="space-between">
      <Box width="50%">
        {totalResults ? (
          times(Math.ceil(totalResults / rowsPerPage), (i: number) => (
            <Box
              key={i}
              className={classNames(classes.pageButton, page === i && classes.selected)}
              onClick={() => setPage(i)}
            >
              {i + 1}
            </Box>
          ))
        ) : (
            <Box
              className={classNames(classes.pageButton, page === 0 && classes.selected)}
              onClick={() => setPage(0)}
            >
              {1}
            </Box>
          )}
      </Box>
      <Box>
        <Box textAlign="center">
          <Typography variant="caption" color="textSecondary">
            {`
              from: ${page * rowsPerPage + 1},
              to: ${(page + 1) * rowsPerPage > totalResults ? totalResults : (page + 1) * rowsPerPage},
              count: ${totalResults},
            `}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}

export default withStyles((theme) => ({
  pageButton: {
    cursor: 'pointer',
    '&:hover': {
      opacity: 0.7,
      backgroundColor: theme.palette.primary.main,
      color: 'white',
    },
    display: `inline-block`,
    height: 24,
    minWidth: 24,
    textAlign: 'center',
  },
  selected: {
    opacity: 1,
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    cursor: 'default',
  },
}))(Pagination);
