import React, { useCallback, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { Box, Typography, Button, CircularProgress } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import Pagination from './Pagination';

import MovieShape from '../../shapes/Movie';

type Props = {
  searchString: string;
  Search: MovieShape[];
  Error: any;
  Results: any;
  totalResults: number;
  loading: boolean;
  fetchMovies: (searchStr: string, page: number) => any;
  page: number;
  newPageLoaded: boolean;
};

function Results({
  classes,
  searchString,
  Search,
  Error,
  Results,
  totalResults,
  loading,
  fetchMovies,
  page,
  newPageLoaded,
}: Props & WithStyles) {
    const setPage = useCallback((pageNo: number) => {
    fetchMovies(searchString, pageNo + 1);
  }, [searchString]);

  useEffect(() => {
    if (newPageLoaded) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    }
  }, [newPageLoaded]);

  if (Error) {
    return (
      <Box display="flex" alignItems="center" justifyContent="center">
        <Typography>{Error}</Typography>
      </Box>
    );
  }
  if (loading) {
    return (
      <Box display="flex" justifyContent="center">
        <CircularProgress />
      </Box>
    );
  }
  return (
    <Box display="flex" justifyContent="center">
      {!!Search.length && (
        <Box display="flex" flexDirection="column" width={600} pb={8}>
          {Search.map((item: MovieShape) => (
            <Box key={item.imdbID} className={classes.itemWrapper}>
              <Box className={classes.posterWrapper}>
                {item.Poster !== "N/A" && (
                  <img src={item.Poster} alt={item.Title} className={classes.poster} />
                )}
              </Box>
              <span>{item.Title}</span>
              <Link to={`/detail/${item.imdbID}`} className={classes.link}>
                <Button variant="contained" color="secondary">Detail</Button>
              </Link>
            </Box>
          ))}
          <Pagination totalResults={totalResults} rowsPerPage={10} page={page - 1} setPage={setPage} />
        </Box>
      )}
    </Box>
  );
}

export default withStyles((theme) => ({
  itemWrapper: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2),
    paddingRight: theme.spacing(15),
    position: 'relative',
    marginBottom: theme.spacing(0.5),
    marginTop: theme.spacing(0.5),
    boxShadow: '1px 1px 10px rgba(0, 0, 0, 0.15)',
  },
  posterWrapper: {
    width: 50,
    height: 50,
    overflow: 'hidden',
  },
  poster: {
    width: 'auto',
    height: '100%',
  },
  link: {
    textDecoration: 'none',
    position: 'absolute',
    right: theme.spacing(2),
  },
}))(Results);
