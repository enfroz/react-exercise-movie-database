import React, { useState, useEffect, useCallback } from 'react';
import { Link, withRouter } from 'react-router-dom';

import { Box, Typography, Button, CircularProgress, Tooltip } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import RatingShape from '../../shapes/Rating';
import MovieShape from '../../shapes/Movie';
import FavouritesShape from '../../shapes/Favourites';

type Props = {
  match: any;
  item: MovieShape;
  favourites: [];
  fetchMovieById: (id: string) => void;
  addToFavourites: (id: string, title: string, poster: string) => void;
};

function Detail({ classes, match, item, favourites, fetchMovieById, addToFavourites }: Props & WithStyles) {
  const [isFavourite, setIsFavourite] = useState(false);

  const handleAddToFavourites = useCallback((id, title, poster) => {
    addToFavourites(id, title, poster);
    setIsFavourite(true);
  }, [addToFavourites]);

  useEffect(() => {
    const { id } = match.params;
    if (favourites.filter((fav: FavouritesShape) => fav.id === id).length > 0) {
      setIsFavourite(true);
    }
    fetchMovieById(id);
  }, [match.params]);

  return (
    <Box position="relative" mt={2}>
      <Box position="absolute" left={0} top={0}>
        <Link to={`/`} className={classes.link}>
          <Button color="primary">back to search</Button>
        </Link>
      </Box>
      <Box pt={10}>
        {item ? (
          <Box>
            <Box mb={2} display="flex" alignItems="center">
              <Typography variant="h3" component="h1">{item.Title}</Typography>
                {isFavourite ? (
                  <Tooltip title="Favourite movie" classes={{ tooltip: classes.tooltip }}>
                    <div>
                      <Button disabled>
                        <StarIcon className={classes.starIcon} />
                      </Button>
                    </div>
                  </Tooltip>
                ) : (
                  <Tooltip title="Add to favourites" classes={{ tooltip: classes.tooltip }}>
                    <Button
                      onClick={() => handleAddToFavourites(item.imdbID, item.Title, item.Poster)}
                      className={classes.favouriteButton}
                      disableRipple
                    >
                      <StarBorderIcon className={classes.starIcon} />
                    </Button>
                  </Tooltip>
                )}
            </Box>
            <Box display="flex" mb={2} mt={2}>
              <Box className={classes.posterWrapper}>
                {item.Poster !== "N/A" && (
                  <img src={item.Poster} alt={item.Title} className={classes.poster} />
                )}
              </Box>
              <Box className={classes.infoWrapper}>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Year: </Typography>
                  <Typography variant="body1">{item.Year}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Rated: </Typography>
                  <Typography variant="body1">{item.Rated}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Released: </Typography>
                  <Typography variant="body1">{item.Released}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Runtime: </Typography>
                  <Typography variant="body1">{item.Runtime}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Genre: </Typography>
                  <Typography variant="body1">{item.Genre}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Director: </Typography>
                  <Typography variant="body1">{item.Director}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Writer: </Typography>
                  <Typography variant="body1">{item.Writer}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Actors: </Typography>
                  <Typography variant="body1">{item.Actors}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Plot: </Typography>
                  <Typography variant="body1">{item.Plot}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Language: </Typography>
                  <Typography variant="body1">{item.Language}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Country: </Typography>
                  <Typography variant="body1">{item.Country}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Awards: </Typography>
                  <Typography variant="body1">{item.Awards}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Ratings: </Typography>
                  {item.Ratings.map((rating: RatingShape) => (
                    <Box key={rating.Source}>
                      <Typography variant="body2" component="span">{`${rating.Source} `}</Typography>
                      <Typography variant="body1" color="secondary" component="span">
                        {rating.Value}
                      </Typography>
                    </Box>
                  ))}
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Metascore: </Typography>
                  <Typography variant="body1">{item.Metascore}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">IMDB rating: </Typography>
                  <Typography variant="body1">{item.imdbRating}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">IMDB votes: </Typography>
                  <Typography variant="body1">{item.imdbVotes}</Typography>
                </Box>
                <Box mb={2}>
                  <a
                    href={`https://www.imdb.com/title/${item.imdbID}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Show on IMDB
                  </a>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Type: </Typography>
                  <Typography variant="body1">{item.Title}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">DVD: </Typography>
                  <Typography variant="body1">{item.Title}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">BoxOffice: </Typography>
                  <Typography variant="body1">{item.Title}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Production: </Typography>
                  <Typography variant="body1">{item.Title}</Typography>
                </Box>
                <Box mb={2}>
                  <Typography variant="caption" color="textSecondary">Website: </Typography>
                  <Typography>
                    <a href={item.Website} target="_blank" rel="noopener noreferrer">
                      {item.Website}
                    </a>
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
        ) : (
          <CircularProgress />
        )}
      </Box>
    </Box>
  );
}

export default withRouter(
  withStyles((theme) => ({
  posterWrapper: {
    maxWidth: 300,
    height: 444,
  },
  infoWrapper: {
    paddingLeft: theme.spacing(10),
  },
  poster: {
    width: 'auto',
    height: 'auto',
  },
  link: {
    textDecoration: 'none',
  },
  favouriteButton: {
    '&.MuiButton-root:hover': {
      backgroundColor: 'transparent',
    },
  },
  starIcon: {
    color: '#ffbf00',
    fontSize: 44,
    '.MuiButton-root:hover &': {
      color: '#ff7600',
    },
  },
  tooltip: {
    fontSize: 14,
  },
  }))(Detail),
);
