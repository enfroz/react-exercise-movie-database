import React from 'react';
import Detail from '../containers/DetailContainer';
import ScrollToTopOnMount from '../components/shared/ScrollToTopOnMount';

function DetailPage() {
  return (
    <>
      <ScrollToTopOnMount />
      <Detail />
    </>
  );
}

export default DetailPage;
