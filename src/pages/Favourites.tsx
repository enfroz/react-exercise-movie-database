import React from 'react';
import Favourites from '../containers/FavouritesContainer';
import ScrollToTopOnMount from '../components/shared/ScrollToTopOnMount';

function FavouritesPage() {
  return (
    <>
      <ScrollToTopOnMount />
      <Favourites />
    </>
  );
}

export default FavouritesPage;
