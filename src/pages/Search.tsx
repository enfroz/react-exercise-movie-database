import React from 'react';
import SearchBox from '../containers/SearchBoxContainer';
import Results from '../containers/ResultsContainer';
import ScrollToTopOnMount from '../components/shared/ScrollToTopOnMount';

function SearchPage() {
  return (
    <>
      <ScrollToTopOnMount />
      <SearchBox />
      <Results />
    </>
  );
}

export default SearchPage;
